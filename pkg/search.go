package pkg

import (
	"fmt"
	"time"
)

type PageParams struct {
	// 第几页
	PageNo int
	// 页面大小
	PageSize int
}

// LogSearch 调度日志搜索类
type LogSearch struct {
	PageParams
	TaskId            string
	JobStartTimeStart string
	JobStartTimeEnd   string
	ExecutorName      string
}

type Time time.Time

func (t Time) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(t).Format(timeFormat))
	return []byte(stamp), nil
}

const (
	timeFormat = "2006-01-02 15:04:05"
)

func (t *Time) UnmarshalJSON(data []byte) (err error) {
	now, err := time.ParseInLocation(`"`+timeFormat+`"`, string(data), time.Local)
	*t = Time(now)
	return
}

// TaskSearch 任务搜索类
type TaskSearch struct {
	PageParams
	TaskName   string
	ExecutorId string
}

// ExecutorSearch 执行器搜索类
type ExecutorSearch struct {
	PageParams
	ExecutorName string
}

// QueryListResponse 查询列表响应类
type QueryListResponse struct {
	// 总数
	Total int64
	// 返回数据
	Data interface{}

	Error string
}

func NewQueryListResponse(total int64, data interface{}, err error) *QueryListResponse {
	msg := "成功"
	if err != nil {
		msg = err.Error()
	}
	return &QueryListResponse{
		Total: total,
		Data:  data,
		Error: msg,
	}
}
