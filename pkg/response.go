package pkg

type Response struct {
	Code int // code = 0 代表成功 -1 代表失败
	Msg  string
	Data interface{}
}

func NewFailResponse(msg string) *Response {
	return &Response{
		Code: -1,
		Msg:  msg,
	}
}

func NewSuccessResponse(msg string) *Response {
	return &Response{
		Code: 0,
		Msg:  msg,
	}
}

func NewSuccessResponseWithData(msg string, data interface{}) *Response {
	return &Response{
		Code: 0,
		Msg:  msg,
		Data: data,
	}
}
