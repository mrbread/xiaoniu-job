package util

import (
	"log"
	"os"
)

// FileAndDirIsExistCommonService 判断文件或文件夹是否存在
func FileAndDirIsExistCommonService(path string) (bool, error) {
	_, erByStat := os.Stat(path)
	if erByStat != nil {
		log.Printf("os stat %s error......%s\n", path, erByStat)
		//该判断主要是部分文件权限问题导致os.Stat()出错,具体看业务启用
		//使用os.IsNotExist()判断为true,说明文件或文件夹不存在
		//if os.IsNotExist(erByStat) {
		//  logs.Error("%s is not exist", erByStat.Error())
		//  return false, erByStat
		//}else{
		//文件/文件夹存在
		//return true, nil
		// }
		return false, erByStat
	}
	return true, nil
}
