package pkg

import "time"

// Task 任务
type Task struct {
	// 任务Id
	Id uint64 `gorm:"primaryKey"`
	// 任务名称
	TaskName string
	// 任务唯一标识
	TaskLabel string
	// 执行器Id
	ExecutorId uint64 `gorm:"index"`
	// 执行器名称
	ExecutorName string
	// 执行策略 默认选择第一个注册地址
	Strategy string
	// 定时策略
	Cron string
	// 状态 1 开启 2 停止
	Status int8
	// 任务参数
	TaskParam string
}

// Executor 执行器
type Executor struct {
	// 执行器Id
	Id uint64 `gorm:"primaryKey"`
	// 名称
	Name string
	// 注册地址,可以多个
	Addresses string
	// 介绍
	Description string
}

// JobLog 任务执行日志
type JobLog struct {
	// 日志Id
	Id uint64 `gorm:"primaryKey"`
	// 任务Id
	TaskId uint64 `gorm:"index"`
	// 任务名称
	TaskName string
	// 执行器名称
	ExecutorName string
	// 执行器地址
	ExecutorAddress string
	// 执行开始时间
	StartTime time.Time
	// 执行结束时间
	EndTime time.Time
	// 调度结果
	ScheduleResult string
	// 执行结果
	Result string
	// 执行状态 0 初始化 1 调度成功 2 调度失败  3 执行成功 4 执行失败
	Status int8
	// 触发类型 0 自动触发 1 手动执行
	TriggerType int8
	// 执行参数
	TaskParam string
}

// JobLogDto 任务执行日志展示类
type JobLogDto struct {
	// 日志Id
	Id uint64 `gorm:"primaryKey"`
	// 任务Id
	TaskId uint64 `gorm:"index"`
	// 任务名称
	TaskName string
	// 执行器名称
	ExecutorName string
	// 执行器地址
	ExecutorAddress string
	// 执行开始时间
	StartTime Time
	// 执行结束时间
	EndTime Time
	// 调度结果
	ScheduleResult string
	// 执行结果
	Result string
	// 执行状态 0 初始化 1 调度成功 2 调度失败  3 执行成功 4 执行失败
	Status int8
	// 触发类型 0 自动触发 1 手动执行
	TriggerType int8
	// 执行参数
	TaskParam string
}

type ClientTaskRequest struct {
	TaskId         uint64
	TaskLabel      string
	LogCallBackUrl string
	// 任务参数 暂时不用
	TaskParam string
}

type ClientLogRequest struct {
	TaskId uint64
}

// ClientResponse 客户端回调服务端接口保存执行结果的响应体
type ClientResponse struct {
	// 执行任务的id
	TaskId     uint64
	TaskResult ClientTaskResult
}

type ClientTaskResult struct {
	// 客户端执行任务结果
	Result string
	// 任务执行结束时间
	FinishTime string
}

type ExecutorSelect struct {
	// 执行器Id
	Id uint64
	// 执行器名称
	Name string
}

type TaskSelect struct {
	// 执行器Id
	Id string
	// 执行器名称
	TaskName string
}

type RunOnceRequest struct {
	// 执行任务的id
	Id uint64
	// 任务参数
	TaskParam string
	// 执行器地址
	ExecutorAddress string
}

// HeartBeatRequest 客户端心跳请求体
type HeartBeatRequest struct {
	// 执行器的IP
	Ip string
	// 执行器的端口
	Port string
	// 执行器的名称
	Name string
	// 类型 1 注册 2 取消注册
	Type int
}

// LoginRequest 登陆请求体
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
