import {Layout, Menu} from 'antd';
import 'antd/dist/antd.css';
import {useState} from "react";

// 引入组件
import HomePage from '@/pages/home';
import Tasks from '@/pages/tasks';
import Executors from '@/pages/executors';
import JobLogs from '@/pages/job_logs';

const {Header, Content} = Layout;

export default function Index() {
    const menuItems = [
        {
            "key": 'homePage',
            "label": "首页"
        }, {
            "key": 'tasks',
            "label": "任务"
        }, {
            "key": 'executors',
            "label": "执行器"
        }, {
            "key": 'jobLogs',
            "label": "调度日志"
        }
    ]

    const [menuIndex, setMenuIndex] = useState('homePage')
    const changeMenu = (e:any) => {
        setMenuIndex(e.key)
    }

    return (
        <Layout className="layout">
            <Header>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={[menuIndex]}
                    items={menuItems}
                    onClick={changeMenu}
                />
            </Header>
            <Content style={{padding: '0 50px',height: 'calc(100vh - 64px)'}}>
                { menuIndex === 'homePage' ? <HomePage/> : null }
                { menuIndex === 'tasks' ? <Tasks/> : null }
                { menuIndex === 'executors' ? <Executors/> : null }
                { menuIndex === 'jobLogs' ? <JobLogs/> : null }
            </Content>
        </Layout>
    );
}
