import {Form, Input, Button, Tabs, message} from 'antd';
import React from 'react';
import 'antd/dist/antd.css';
import styles from './login.less';
import request from "umi-request";
import {history} from "@@/exports";

const onFinish = (values: any) => {
    request
        .post("/api/login", {
            data: values,
        })
        .then((res) => {
            if(res.status == "success"){
                message.success(res.message);
                localStorage.setItem("token",res.token);
                history.push("/");
            }else{
                message.error(res.message);
            }
        })
        .catch((error) => {
            message.error("系统异常！").then((r) => {
                console.log(r);
            });
        });
};

export default function Login() {
    const items = [{
        label:'账户密码登陆',
        key : '1',
        children: (<Form
            name="normal_login"
            initialValues={{ remember: true }}
            onFinish={onFinish}
        >
            <Form.Item
                name="username"
                rules={[{ required: true, message: '请输入用户名!' }]}
                style={{ borderBottom: '1px solid #DCDCDC' }}
            >
                <Input placeholder="请输入用户名" bordered={false} />
            </Form.Item>
            <Form.Item
                name="password"
                rules={[{ required: true, message: '请输入密码!' }]}
                style={{ borderBottom: '1px solid #DCDCDC' }}
            >
                <Input
                    bordered={false}
                    type="password"
                    placeholder="请输入密码"
                />
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit" block
                        style={{ height: '56PX', borderRadius: '12PX',marginTop: '30PX'}}>
                    登录
                </Button>
            </Form.Item>
        </Form>)
    }]
    return (
        <div className={styles.bg}>
            <div className={styles.login_card}>
                <Tabs type="card" defaultActiveKey="1" centered style={{ margin: '0 auto' }}
                items={items}>
                </Tabs>

            </div>
        </div>
    )
}
