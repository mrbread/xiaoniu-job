export default function HomePage() {
  return (
    <div>
      <h2>Yay! Welcome to xiaoniu-job dashboard!</h2>
      <h2>欢迎使用菜牛任务调度系统</h2>
      <p>
        You can control the tasks , view the logs and set the executors with the dashboard!
      </p>
      <p>
        <strong>Anyway , let's enjoy！</strong>
      </p>
    </div>
  );
}
