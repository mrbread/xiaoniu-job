import { Navigate, Outlet } from "umi";

export default (props: any) => {
    const token = localStorage.getItem("token");
    if (token) {
        return <Outlet />;
    } else {
        return <Navigate to="/login" />;
    }
};