export default {
  npmClient: "yarn",
  proxy: {
    "/api/": {
      target: "http://127.0.0.1:9797",
      changeOrigin: true,
      pathRewrite: { "^/api": "/api" },
    },
  },
  routes: [
    { path: "/", component: "index", wrappers: ["@/wrappers/auth"],
      routes: [
        { path: '/index', component: 'index',wrappers: ["@/wrappers/auth"],},
        { path: '/executors', component: 'executors',wrappers: ["@/wrappers/auth"],},
        { path: '/home', component: 'home',wrappers: ["@/wrappers/auth"],},
        { path: '/job_logs', component: 'job_logs',wrappers: ["@/wrappers/auth"],},
        { path: '/tasks', component: 'tasks',wrappers: ["@/wrappers/auth"],},
      ],
    },
    { path: "/login", component: "login" },
  ],
};
