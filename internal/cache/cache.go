package cache

import "xiaoniu-job/pkg"

// CACHE 保存任务执行结果上传Server失败的执行结果 后续避免本地缓存过多，需要采取LRU策略
var CACHE = make(map[uint64]pkg.ClientTaskResult)
