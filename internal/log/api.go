package log

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"xiaoniu-job/internal/db"
	"xiaoniu-job/pkg"
)

// ListHandler 查询调度日志列表
func ListHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
		return
	}
	var entity pkg.LogSearch
	err = json.Unmarshal(res, &entity)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
		return
	}
	marshal, err := json.Marshal(db.GetLogList(&entity))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("响应转换异常"))
		return
	}
	w.Write(marshal)

}
