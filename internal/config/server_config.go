package config

import (
	"gopkg.in/ini.v1"
	"log"
	"strconv"
)

// 服务端全局配置文件

type ServerConfiguration struct {
	// 数据库地址
	DataBaseHost string
	// 数据库端口
	DataBasePort int
	// 数据库
	DataBaseName string
	// 数据库yonghu
	DataBaseUser string
	// 数据库密码
	DataBasePwd string
	// 启动服务名
	AppName string
	// 启动端口
	AppPort int
	// 任务调度中心的IP
	AppIp string
	// 登录用户名
	LoginUser string
	// 登录密码
	LoginPwd string
}

// ServerRation 全局配置
var ServerRation = new(ServerConfiguration)

// InitServerConfig 加载config/server.ini文件的配置信息
func InitServerConfig() bool {
	// 读取配置文件
	//加载配置文件
	cfg, err := ini.Load("config/server.ini")

	if err != nil {
		log.Fatalf("App settings init error, fail to parse 'config/server.ini': %v", err)
		return false
	}

	dbHost := cfg.Section("db").Key("host").Value()
	dbName := cfg.Section("db").Key("dbName").Value()
	dbUser := cfg.Section("db").Key("user").Value()
	dbPwd := cfg.Section("db").Key("pwd").Value()
	dbPort := cfg.Section("db").Key("port").Value()
	appName := cfg.Section("app").Key("name").Value()
	appPort := cfg.Section("app").Key("port").Value()
	appIp := cfg.Section("app").Key("ip").Value()
	loginUser := cfg.Section("app").Key("loginUser").Value()
	loginPwd := cfg.Section("app").Key("loginPwd").Value()
	ServerRation.AppPort, _ = strconv.Atoi(appPort)
	ServerRation.AppName = appName
	ServerRation.AppIp = appIp
	ServerRation.DataBasePort, _ = strconv.Atoi(dbPort)
	ServerRation.DataBaseHost = dbHost
	ServerRation.DataBaseName = dbName
	ServerRation.DataBaseUser = dbUser
	ServerRation.DataBasePwd = dbPwd
	ServerRation.LoginUser = loginUser
	ServerRation.LoginPwd = loginPwd

	return true
}
