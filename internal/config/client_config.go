package config

import (
	"gopkg.in/ini.v1"
	"log"
	"os"
	"xiaoniu-job/pkg/util"
)

type ClientConfiguration struct {
	// 日志路径
	LogPath string
	// 服务端地址
	AdminServer string
	// 尝试次数
	TryCount string
	// 启动端口
	Port string
	// 执行器名称
	ExecutorName string
}

// ClientRation 全局配置
var ClientRation = new(ClientConfiguration)

// InitClientConfig 加载config/client.ini文件的配置信息
func InitClientConfig() bool {
	// 读取配置文件
	//加载配置文件
	cfg, err := ini.Load("config/client.ini")

	if err != nil {
		log.Fatalf("App settings init error, fail to parse 'config/client.ini': %v", err)
	}

	logPath := cfg.Section("config").Key("logPath").Value()
	adminServer := cfg.Section("config").Key("adminServer").Value()
	tryCount := cfg.Section("config").Key("tryCount").Value()
	ClientRation.LogPath = logPath
	ClientRation.AdminServer = adminServer
	ClientRation.TryCount = tryCount
	ClientRation.Port = cfg.Section("config").Key("port").Value()
	ClientRation.ExecutorName = cfg.Section("config").Key("executorName").Value()

	if ClientRation.Port == "" {
		log.Fatalf("The startup port is not configured!")
	}

	if service, _ := util.FileAndDirIsExistCommonService(logPath + "/task"); !service {
		os.MkdirAll(logPath+"/task/", 0777)
	}

	return true
}
