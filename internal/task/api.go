package task

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"xiaoniu-job/internal/cron"
	"xiaoniu-job/internal/db"
	"xiaoniu-job/pkg"
)

// 任务的创建 、更新、删除

func AddHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
		return
	}
	var task pkg.Task
	err = json.Unmarshal(res, &task)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
		return
	}
	cnt, err := db.AddTask(&task)
	if cnt > 0 {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("新增任务成功"))
	} else {
		w.WriteHeader(http.StatusBadRequest)
		var errMsg string
		if err != nil {
			errMsg = err.Error()
		}
		w.Write([]byte("新增任务失败，错误信息为:" + errMsg))
	}
}

func UpdateHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
	}
	var task pkg.Task
	err = json.Unmarshal(res, &task)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
	}
	cnt, err := db.UpdateTask(&task)
	if cnt > 0 {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("更新任务成功"))
	} else {
		w.WriteHeader(http.StatusBadRequest)
		var errMsg string
		if err != nil {
			errMsg = err.Error()
		}
		w.Write([]byte("更新任务失败，错误信息为:" + errMsg))
	}
}

func DeleteHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
	}
	var task pkg.Task
	err = json.Unmarshal(res, &task)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
	}
	cnt, err := db.DeleteTask(task.Id)
	if cnt > 0 {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("删除任务成功"))
	} else {
		w.WriteHeader(http.StatusBadRequest)
		var errMsg string
		if err != nil {
			errMsg = err.Error()
		}
		w.Write([]byte("删除任务失败，错误信息为:" + errMsg))
	}
}

// 执行一次任务

func ExecuteOnceHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
	}
	var task pkg.Task
	err = json.Unmarshal(res, &task)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
	}

}

// ListHandler 查询任务列表
func ListHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
		return
	}
	var entity pkg.TaskSearch
	err = json.Unmarshal(res, &entity)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
		return
	}
	marshal, err := json.Marshal(db.GetTaskList(&entity))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("响应转换异常"))
		return
	}
	w.Write(marshal)
}

// RunOnceHandler 执行一次的逻辑处理
func RunOnceHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("参数有误"))
		return
	}
	var entity pkg.RunOnceRequest
	err = json.Unmarshal(res, &entity)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("JSON转换异常"))
		return
	}
	// 执行一次
	task, err := db.GetTaskById(entity.Id)
	if err != nil {
		w.Write([]byte("执行失败" + err.Error()))
	} else {
		go cron.SendTaskToClient(task, 1, entity.TaskParam, entity.ExecutorAddress)
		w.Write([]byte("执行成功！"))
	}
}

func SelectHandler(w http.ResponseWriter, r *http.Request) {
	marshal, err := json.Marshal(db.GetTaskMapForSelect())
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("响应转换异常"))
		return
	}
	w.Write(marshal)

}
