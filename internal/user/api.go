package user

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"xiaoniu-job/internal/config"
	"xiaoniu-job/pkg"
)

// LoginHandler 登陆控制
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message":"参数有误"}`))
		return
	}
	var req pkg.LoginRequest
	err = json.Unmarshal(res, &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"message":"JSON转换异常"}`))
		return
	}

	// 校验登录名和登录密码是否跟配置文件的一致
	if req.Username == config.ServerRation.LoginUser && req.Password == config.ServerRation.LoginPwd {
		w.Write([]byte(`{"status":"success", "token":"random-string", "message":"登陆成功"}`))
	} else {
		w.Write([]byte(`{"status":"fail", "message":"用户名或密码错误"}`))
	}

}
