git clone https://gitee.com/mrbread/xiaoniu-job

docker build -t xiaoniu-job/client:1.0.0 -f .\dockerfiles\client_dockerfile .

docker build -t xiaoniu-job/server:1.0.0 -f .\dockerfiles\server_dockerfile .

docker build -t xiaoniu-job/frontend:1.0.0 -f .\dockerfiles\frontend_dockerfile .

del /F /S /Q .\xiaoniu-job