package main

import (
	"context"
	"log"
	"net/http"
	"time"
	"xiaoniu-job/internal/api"
	"xiaoniu-job/internal/beat"
	"xiaoniu-job/internal/biz"
	"xiaoniu-job/internal/config"
	"xiaoniu-job/internal/job"
	"xiaoniu-job/pkg"
)

// 客户端启动类
func main() {
	// 加载配置
	initConfig := config.InitClientConfig()
	if !initConfig {
		log.Fatalln("App config file read error!")
		return
	}
	// 开启后台定时任务
	go biz.StartConsumer()

	// 心跳任务
	go beat.StartHeartBeatJob()

	// 注册任务
	job.RegisterTask("job.Task1", Task1)

	mux := http.NewServeMux()
	mux.HandleFunc("/run", api.RunHandler)
	mux.HandleFunc("/getLog", api.GetLogHandler)
	mux.HandleFunc("/queryLocalLog", api.QueryLocalLogHandler)
	mux.HandleFunc("/gracefulShutdown", api.GracefulShutdownHandler)
	server := &http.Server{
		Addr:    ":" + config.ClientRation.Port,
		Handler: mux,
	}
	go func() {
		// 启动整个程序
		server.ListenAndServe()
	}()

	log.Println("client is already running....")

	// 如果没有收到所有任务都完成的信号，会一直阻塞在这里
	<-biz.CompleteChan
	// 优雅停机
	server.Shutdown(context.Background())
}

func Task1(taskLog *log.Logger, request *pkg.ClientTaskRequest) string {
	taskLog.Println("=====业务逻辑打印=======")
	time.Sleep(time.Second * 30)
	return "业务逻辑处理完成"
}
