module xiaoniu-job

go 1.17

require (
	github.com/robfig/cron/v3 v3.0.1
	gopkg.in/ini.v1 v1.66.4
	gorm.io/driver/mysql v1.3.3
	gorm.io/gorm v1.23.5
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
)
